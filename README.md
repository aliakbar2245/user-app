# Getting Started

For further reference, please consider the following sections:

1. In this project, using docker as a containerization. Please download [here to Docker Desktop](https://www.docker.com/products/docker-desktop/) and install
2. Change the `.env.template` to `.env` in the root project

# Installation

Provide step-by-step instructions on how to install and run your project. Include any prerequisites and dependencies.

1. Make sure your docker installation according to your OS and running well. Example in windows section :
   - Docker running in taskbar ![docker_running_windows](md_image/docke_running_windows.PNG)<br/><br/>
   - Docker list image ![docker_image_windows](md_image/docker_image_windows.PNG)<br/><br/>
2. Type in terminal `docker compose up --build userdb -d` wait for it until done. `userdb` is a database service name define in `docker-compose.yml`
3. Type in terminal `docker compose up --build userapp -d` wait for it until done. `userapp` is a spring-boot application name define in `docker-compose.yml`
4. Done

# Security Api

In this project has `org.springframework.security` to secure the several API fuction filter. So, to call the API you need the _**Header**_ `Authorization` mention/tag in process like below :

1. Sample `POST` API not required permission :

   - Payload :

   ```
    POST /v1/api/auth/login HTTP/1.1
    Host: localhost:8383
    Content-Type: application/json
    Content-Length: 67

    {
        "email":"aliakbar@gmail.com",
        "password":"@1wert\$ww"
    }
   ```

   - OK Status (200 OK) : No token validation because permission wasn't required ![OK](md_image/token_validation_success.PNG) <br/><br/>

2. etc
