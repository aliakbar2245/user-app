create table users (
    id varchar(255) not null primary key,
    created_by varchar(255) not null,
    created_date timestamptz default current_timestamp not null,
    updated_by varchar(255) not null,
    updated_date timestamptz default current_timestamp,
    deleted_date timestamptz,
    version int default 0,
    name varchar(255) not null,
    email varchar(255) not null,
    password text not null,
    expire_token_date timestamptz
);