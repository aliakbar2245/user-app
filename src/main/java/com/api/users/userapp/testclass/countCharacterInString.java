package com.api.users.userapp.testclass;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class countCharacterInString {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        String input = scan.nextLine();
        char[] cwords = input.toCharArray();

        Map<Character, Integer> tempChar = new HashMap<>();

        for(char c : cwords){
            //only support alphabetic
            if(Character.isAlphabetic(c)){
                c = Character.toLowerCase(c);
                tempChar.put(c, tempChar.getOrDefault(c, 0) + 1);
            }else{
                tempChar.put(c, tempChar.getOrDefault(c, 0) + 1);
            }
        }

        for(Map.Entry<Character, Integer> in : tempChar.entrySet()){
            System.out.println("jumlah character '"+in.getKey()+"' : "+in.getValue());
        }
    }
}

