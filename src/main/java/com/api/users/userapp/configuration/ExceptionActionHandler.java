package com.api.users.userapp.configuration;

import com.api.users.userapp.common.GlobalEntityResponse;
import com.api.users.userapp.exceptions.AlreadyExitsException;
import com.api.users.userapp.exceptions.BadRequestException;
import com.api.users.userapp.exceptions.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
@Slf4j
public class ExceptionActionHandler extends ResponseEntityExceptionHandler  {

    @ExceptionHandler(value = Exception.class)
    protected ResponseEntity<Object> handleAllException(Exception ex, WebRequest request) {
        GlobalEntityResponse<Object> bodyOfResponse = GlobalEntityResponse.nok(null, HttpStatus.INTERNAL_SERVER_ERROR, ex);
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        if (ex instanceof BadRequestException) {
            bodyOfResponse = GlobalEntityResponse.nok(null, HttpStatus.BAD_REQUEST, ex);
            status = HttpStatus.BAD_REQUEST;
        } else if (ex instanceof NotFoundException) {
            bodyOfResponse = GlobalEntityResponse.nok(null, HttpStatus.NOT_FOUND, ex);
            status = HttpStatus.NOT_FOUND;
        }else if (ex instanceof AlreadyExitsException) {
            bodyOfResponse = GlobalEntityResponse.nok(null, HttpStatus.CONFLICT, ex);
            status = HttpStatus.CONFLICT;
        }
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), status, request);
    }
}
