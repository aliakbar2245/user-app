package com.api.users.userapp.configuration;

import com.api.users.userapp.adapter.app.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import java.util.Optional;

@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorProvider")
public class JpaAuditingConfiguration {

  @Autowired
  private UserService userInfoService;

  @Bean
  public AuditorAware<String> auditorProvider() {
    return () -> {
      String updateBy = "SYSTEM";
      if (userInfoService.getInfo().isPresent()) {
        updateBy = userInfoService.getInfo().get().getUserName();
      }
      return Optional.of(updateBy);
    };
  }
}
