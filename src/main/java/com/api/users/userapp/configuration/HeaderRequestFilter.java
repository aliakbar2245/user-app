package com.api.users.userapp.configuration;

import com.api.users.userapp.common.JwtTokenUtil;
import com.api.users.userapp.entity.SessionUser;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class HeaderRequestFilter extends OncePerRequestFilter {

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException, AccessDeniedException {
		String requestUserHeader = request.getHeader("Authorization");
		if (StringUtils.isEmpty(requestUserHeader) || !(requestUserHeader.startsWith("Bearer ") || requestUserHeader.startsWith("jwt "))) {
			chain.doFilter(request,response);
			return ;
		}

		String jwtToken = requestUserHeader.substring(requestUserHeader.indexOf(" "));
		SessionUser sessionUser = jwtTokenUtil.extractAndValidateToken(jwtToken);
		if ( sessionUser == null) {
			chain.doFilter(request,response);
			return;
		}

		//Once we get the token validate it.
		String email = sessionUser.getUserName();
		List<GrantedAuthority> list = new ArrayList<>();
		User userDetails = new User(email, "", list);

		UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
				userDetails, null,
				userDetails.getAuthorities());
		SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);

		log.info("[auth-filter] user-details email:{}, expiration:{}", sessionUser.getUserName(), sessionUser.getTokenExpiredDate());
		chain.doFilter(request, response);
	}
}
