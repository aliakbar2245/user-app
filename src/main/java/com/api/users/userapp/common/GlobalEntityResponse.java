package com.api.users.userapp.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GlobalEntityResponse<T> {
    private String status;
    private Integer code;
    private T data;
    private String message;
    private Object pagination;

    public static <T> GlobalEntityResponse<T> ok(T data) {
        GlobalEntityResponse<T> response = new GlobalEntityResponse<T>();
        response.code = HttpStatus.OK.value();
        response.message = "success";
        response.status = "" + HttpStatus.OK;
        response.data = data;
        return response;
    }

    public static <T> GlobalEntityResponse<T> okPage(T data, Long totalElement) {
        GlobalEntityResponse<T> response = new GlobalEntityResponse<T>();
        response.code = HttpStatus.OK.value();
        response.message = "success";
        response.status = "" + HttpStatus.OK;
        response.data = data;
        Map<String, Object> pageMap = new HashMap<>();
        pageMap.put("totalElement", totalElement);
        response.pagination = pageMap;
        return response;
    }

    public static <T> GlobalEntityResponse<T> nok(T data, HttpStatus httpStatus, Exception exception) {
        GlobalEntityResponse<T> response = new GlobalEntityResponse<T>();
        response.code = httpStatus.value();
        response.message = exception.getMessage();
        response.status = "" + httpStatus;
        response.data = data;
        return response;
    }
}
