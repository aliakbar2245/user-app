package com.api.users.userapp.common;

import com.api.users.userapp.entity.SessionUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Header;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.*;
import java.util.function.Function;

@Slf4j
@Component
public class JwtTokenUtil implements Serializable {
	public static final long JWT_TOKEN_VALIDITY = 24*60*60;
	@Value("${jwt.secret}")
	private String secret;


	public SessionUser extractAndValidateToken(String token) {
		try {
			Function<Claims, String> claimEmail = Claims::getSubject;
			Function<Claims, Date> claimExpiration = Claims::getExpiration;
			Claims claims = Jwts.parser()
					.setSigningKey(secret.getBytes())
					.parseClaimsJws(token)
					.getBody();
			SessionUser user = new SessionUser();
			user.setUserName(claimEmail.apply(claims));
			Date expiration = claimExpiration.apply(claims);
			user.setTokenExpiredDate(expiration);
			return expiration.after(new Date())?user:null;

		} catch (Exception e) {
			log.error("error extract and validate the token: " + e.getMessage());
			return null;
		}
	}

	public SessionUser doGenerateToken(User user) {
		//header map
		Map<String, Object> mapHeader = new HashMap<>();
		mapHeader.put("typ", Header.JWT_TYPE);
		mapHeader.put("alg", "HS512");

		Date expiration = new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY*1000);
		String token = Jwts.builder()
				.setHeader(mapHeader)
				.setSubject(user.getUsername())
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(expiration)
				.signWith(SignatureAlgorithm.HS512, secret.getBytes())
				.compact();

		SessionUser sessionUser = new SessionUser();
		sessionUser.setTokenExpiredDate(expiration);
		sessionUser.setToken(token);
		return sessionUser;
	}
}
