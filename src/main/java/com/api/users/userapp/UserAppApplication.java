package com.api.users.userapp;

import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.StandardEnvironment;

import io.github.cdimascio.dotenv.Dotenv;
import io.github.cdimascio.dotenv.DotenvEntry;
import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@Slf4j
public class UserAppApplication {

	public static void main(String[] args) {
		try {
			Map<String, Object> dotenv = Dotenv.configure().load().entries()
					.stream().collect(Collectors.toMap(DotenvEntry::getKey, DotenvEntry::getValue));
			new SpringApplicationBuilder(UserAppApplication.class)
					.environment(new StandardEnvironment() {
						@Override
						protected void customizePropertySources(MutablePropertySources propertySources) {
							super.customizePropertySources(propertySources);
							propertySources.addLast(new MapPropertySource("dotenvProperties", dotenv));
						}
					}).run(args);
		} catch (Exception e) {
			log.info("no .env file found, using server's environment variable");
			SpringApplication.run(UserAppApplication.class, args);
		}
	}

}
