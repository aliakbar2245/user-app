package com.api.users.userapp.entity;

import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Data
public class SessionUser {
    private String userName;
    private Date tokenExpiredDate;
    private String token;
}
