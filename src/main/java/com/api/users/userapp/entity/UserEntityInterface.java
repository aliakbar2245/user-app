package com.api.users.userapp.entity;

import org.springframework.beans.factory.annotation.Value;

import java.util.Date;

public interface UserEntityInterface {

    @Value("#{target.id}")
    String getUserId();

    @Value("#{target.name}")
    String getName();

    @Value("#{target.email}")
    String getEmail();

    @Value("#{target.password}")
    String getPassword();

    @Value("#{target.expire_token_date}")
    Date getExpireTokenDate();
}
