package com.api.users.userapp.adapter.app.in;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;

@Setter
@Getter
@Builder
public class LoginRequestApp {
    private String email;
    private String password;
}
