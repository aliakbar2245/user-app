package com.api.users.userapp.adapter.app.service;

import com.api.users.userapp.adapter.app.in.LoginRequestApp;
import com.api.users.userapp.adapter.web.response.LoginResponseWeb;
import com.api.users.userapp.exceptions.BadRequestException;
import com.api.users.userapp.exceptions.NotFoundException;

import java.util.Map;

public interface AuthenticateService {
    LoginResponseWeb authenticate(LoginRequestApp command) throws NotFoundException;
}
