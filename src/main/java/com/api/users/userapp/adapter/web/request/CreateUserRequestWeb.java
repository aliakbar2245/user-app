package com.api.users.userapp.adapter.web.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Setter
@Getter
public class CreateUserRequestWeb {
    private String name;
    @Email(message = "email format required")
    private String email;
    @NotEmpty(message = "password is required")
    private String password;
}
