package com.api.users.userapp.adapter.web.response;

import com.api.users.userapp.entity.SessionUser;
import com.api.users.userapp.entity.UserEntity;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Builder
@Setter
@Getter
public class LoginResponseWeb {
    private String name;
    private String email;
    private String token;
    private Date expireToken;

    public static LoginResponseWeb fromEntity(UserEntity userEntity, String token){
        return LoginResponseWeb.builder()
                .email(userEntity.getEmail())
                .name(userEntity.getName())
                .expireToken(userEntity.getExpireTokenDate())
                .token(token)
                .build();
    }
}
