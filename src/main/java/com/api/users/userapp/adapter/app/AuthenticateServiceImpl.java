package com.api.users.userapp.adapter.app;

import com.api.users.userapp.adapter.app.in.LoginRequestApp;
import com.api.users.userapp.adapter.app.repository.UserRepository;
import com.api.users.userapp.adapter.app.service.AuthenticateService;
import com.api.users.userapp.adapter.web.response.LoginResponseWeb;
import com.api.users.userapp.common.JwtTokenUtil;
import com.api.users.userapp.entity.SessionUser;
import com.api.users.userapp.entity.UserEntity;
import com.api.users.userapp.entity.UserEntityInterface;
import com.api.users.userapp.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class AuthenticateServiceImpl implements AuthenticateService {

    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional(rollbackFor = {Exception.class})
    public LoginResponseWeb authenticate(LoginRequestApp command) throws NotFoundException {
        //validate user
        Optional<UserEntityInterface> userExists = userRepository.findUserByEmail(command.getEmail());
        if(!userExists.isPresent()){
            throw new NotFoundException("[INVALID AUTHENTICATION] email/password invalid");
        }
        String passwordDb = userExists.get().getPassword();
        if(!passwordEncoder.matches(command.getPassword(), passwordDb)){
            throw new NotFoundException("[INVALID AUTHENTICATION] email/password invalid");
        }

        //generate token
        User user = new User(command.getEmail(), command.getPassword(), new ArrayList<>());
        SessionUser sessionUser = jwtTokenUtil.doGenerateToken(user);

        //update expired token user
        userRepository.updateUserExpiredDateToken(sessionUser.getTokenExpiredDate(), userExists.get().getUserId());
        return LoginResponseWeb.fromEntity(UserEntity.builder()
                        .name(userExists.get().getName())
                        .email(userExists.get().getEmail())
                        .password(userExists.get().getPassword())
                        .expireTokenDate(sessionUser.getTokenExpiredDate())
                .build(), sessionUser.getToken());
    }
}
