package com.api.users.userapp.adapter.app.in;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class UpdateUserRequestApp {
    private String userId;
    private String name;
    private String password;
}
