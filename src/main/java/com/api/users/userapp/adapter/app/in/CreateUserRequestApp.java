package com.api.users.userapp.adapter.app.in;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;

@Builder
@Setter
@Getter
public class CreateUserRequestApp {
    private String email;
    private String name;
    private String password;
}
