package com.api.users.userapp.adapter.app;

import com.api.users.userapp.adapter.app.in.CreateUserRequestApp;
import com.api.users.userapp.adapter.app.in.FetchUserRequestApp;
import com.api.users.userapp.adapter.app.in.UpdateUserRequestApp;
import com.api.users.userapp.adapter.app.repository.UserRepository;
import com.api.users.userapp.adapter.app.repository.UserSpecification;
import com.api.users.userapp.adapter.app.service.UserService;
import com.api.users.userapp.entity.SessionUser;
import com.api.users.userapp.entity.UserEntity;
import com.api.users.userapp.entity.UserEntityInterface;
import com.api.users.userapp.exceptions.AlreadyExitsException;
import com.api.users.userapp.exceptions.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Optional;

@Slf4j
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional(rollbackFor = {Exception.class})
    public void create(CreateUserRequestApp command) throws AlreadyExitsException {
        //validate email exists
        Optional<UserEntityInterface> userExists = userRepository.findUserByEmail(command.getEmail());
        if(userExists.isPresent()){
            throw new AlreadyExitsException("[CONFLICT DATA] user email already exist!");
        }
        UserEntity newUser = new UserEntity();
        newUser.setEmail(command.getEmail());
        newUser.setName(command.getName());
        newUser.setPassword(passwordEncoder.encode(command.getPassword()));
        userRepository.save(newUser);
    }

    @Override
    public Optional<SessionUser> getInfo() {
        try {
            SessionUser sessionUser = null;
            String userNameSession = SecurityContextHolder.getContext().getAuthentication().getName();
            if(!"anonymousUser".equalsIgnoreCase(userNameSession)){
                sessionUser.setUserName(userNameSession);
            }
            return Optional.of(sessionUser);
        } catch (Exception e){
            return Optional.empty();
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserEntity> fetchPagination(FetchUserRequestApp filter) {
        return userRepository.findAll(new UserSpecification(filter),
                PageRequest.of(filter.getPage(), filter.getSize(), filter.getDirection(), filter.getSortBy()));
    }

    @Override
    @Transactional(rollbackFor = {Exception.class})
    public void update(UpdateUserRequestApp command) throws NotFoundException {
        //validate email exists
        Optional<UserEntityInterface> userExists = userRepository.findUserById(command.getUserId());
        if(!userExists.isPresent()){
            throw new NotFoundException("[NOT_FOUND] user information not exists!");
        }

        //if new password isn't empty then save as new password else save existing
        String password = StringUtils.isNotEmpty(command.getPassword()) ?
                passwordEncoder.encode(command.getPassword()) : userExists.get().getPassword();
        String name = StringUtils.isNotEmpty(command.getName())?command.getName():"";

        //update existing user information
        userRepository.updateUserInformation(command.getUserId(), name, password);
    }

    @Override
    @Transactional(rollbackFor = {Exception.class})
    public void delete(String userId) throws NotFoundException {
        //validate email exists
        Optional<UserEntityInterface> userExists = userRepository.findUserById(userId);
        if(!userExists.isPresent()){
            throw new NotFoundException("[NOT_FOUND] user information not exists!");
        }

        //soft delete user account
        userRepository.softDeleteUserAccount(userId, new Date());
    }
}
