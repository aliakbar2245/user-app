package com.api.users.userapp.adapter.web;

import com.api.users.userapp.adapter.app.in.CreateUserRequestApp;
import com.api.users.userapp.adapter.app.in.FetchUserRequestApp;
import com.api.users.userapp.adapter.app.in.UpdateUserRequestApp;
import com.api.users.userapp.adapter.app.service.UserService;
import com.api.users.userapp.adapter.web.request.CreateUserRequestWeb;
import com.api.users.userapp.adapter.web.request.UpdateUserRequestWeb;
import com.api.users.userapp.adapter.web.response.UserResponseWeb;
import com.api.users.userapp.common.GlobalEntityResponse;
import com.api.users.userapp.entity.UserEntity;
import com.api.users.userapp.exceptions.AlreadyExitsException;
import com.api.users.userapp.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/v1/api/user")
public class UserController {
    @Autowired
    private UserService userService;

    /**
     * @param request
     * @return
     * @throws AlreadyExitsException
     *
     * Register User with no permission token
     */
    @PostMapping(value = "/register", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public GlobalEntityResponse<String> create(@Valid @RequestBody CreateUserRequestWeb request) throws AlreadyExitsException {
        userService.create(CreateUserRequestApp.builder()
                        .email(request.getEmail())
                        .name(request.getName())
                        .password(request.getPassword())
                .build());
        return GlobalEntityResponse.ok("created user success!");
    }

    /**
     * @param page
     * @param size
     * @param sortBy
     * @param direction
     * @param email
     * @return
     */
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public GlobalEntityResponse<List<UserResponseWeb>> getPagination(@RequestParam(value = "page", defaultValue = "0") int page,
                                                                     @RequestParam(value = "size", defaultValue = "10") int size,
                                                                     @RequestParam(value = "sortBy", defaultValue = "name") String sortBy,
                                                                     @RequestParam(value = "direction", defaultValue = "ASC") Sort.Direction direction,
                                                                     @RequestParam(value = "email", required = false) String email,
                                                                     @RequestParam(value = "name", required = false) String name){
        Page<UserEntity> userEntities = userService.fetchPagination(FetchUserRequestApp.builder()
                        .page(page)
                        .size(size)
                        .sortBy(sortBy)
                        .direction(direction)
                        .email(email)
                        .name(name)
                .build());
        return GlobalEntityResponse.okPage(userEntities.getContent().stream()
                .map(UserResponseWeb::fromEntity)
                .collect(Collectors.toList()), userEntities.getTotalElements());
    }

    /**
     * @param requestWeb
     * @return
     * @throws NotFoundException
     *
     * Update User Account Information
     */
    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public GlobalEntityResponse<String> update(@Valid @RequestBody UpdateUserRequestWeb requestWeb) throws NotFoundException{
        userService.update(UpdateUserRequestApp.builder()
                        .userId(requestWeb.getUserId())
                        .name(requestWeb.getName())
                        .password(requestWeb.getPassword())
                .build());
        return GlobalEntityResponse.ok("updated user account success!");
    }

    /**
     * @param userId
     * @return
     * @throws NotFoundException
     *
     * Soft Delete User Account
     */
    @DeleteMapping(value = "/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public GlobalEntityResponse<String> delete(@PathVariable(value = "userId") String userId) throws NotFoundException{
        userService.delete(userId);
        return GlobalEntityResponse.ok("deleted user account success!");
    }

}
