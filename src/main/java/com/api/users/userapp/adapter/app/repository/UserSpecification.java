package com.api.users.userapp.adapter.app.repository;

import com.api.users.userapp.adapter.app.in.FetchUserRequestApp;
import com.api.users.userapp.entity.UserEntity;
import com.api.users.userapp.entity.UserEntity_;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.parameters.P;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class UserSpecification implements Specification<UserEntity> {
    private final transient FetchUserRequestApp filter;

    public UserSpecification(FetchUserRequestApp filter) {
        this.filter = filter;
    }

    @Override
    public Predicate toPredicate(Root<UserEntity> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();
        if(StringUtils.isNotEmpty(filter.getEmail())){
            predicates.add(criteriaBuilder.equal(root.get(UserEntity_.EMAIL), filter.getEmail()));
        }
        if(StringUtils.isNotEmpty(filter.getName())){
            predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get(UserEntity_.NAME)), "%"+filter.getName()+"%"));
        }
        predicates.add(criteriaBuilder.isNull(root.get(UserEntity_.DELETED_DATE)));
        return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
    }
}
