package com.api.users.userapp.adapter.app.service;

import com.api.users.userapp.adapter.app.in.CreateUserRequestApp;
import com.api.users.userapp.adapter.app.in.FetchUserRequestApp;
import com.api.users.userapp.adapter.app.in.LoginRequestApp;
import com.api.users.userapp.adapter.app.in.UpdateUserRequestApp;
import com.api.users.userapp.adapter.web.response.LoginResponseWeb;
import com.api.users.userapp.entity.SessionUser;
import com.api.users.userapp.entity.UserEntity;
import com.api.users.userapp.exceptions.AlreadyExitsException;
import com.api.users.userapp.exceptions.NotFoundException;
import org.springframework.data.domain.Page;

import java.util.Optional;

public interface UserService {
    void create(CreateUserRequestApp command) throws AlreadyExitsException;
    Optional<SessionUser> getInfo();
    Page<UserEntity> fetchPagination(FetchUserRequestApp filter);
    void update(UpdateUserRequestApp command) throws NotFoundException;
    void delete(String userId) throws NotFoundException;
}
