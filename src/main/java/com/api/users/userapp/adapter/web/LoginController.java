package com.api.users.userapp.adapter.web;

import com.api.users.userapp.adapter.app.in.LoginRequestApp;
import com.api.users.userapp.adapter.app.service.AuthenticateService;
import com.api.users.userapp.adapter.web.request.LoginRequestWeb;
import com.api.users.userapp.common.GlobalEntityResponse;
import com.api.users.userapp.adapter.web.response.LoginResponseWeb;
import com.api.users.userapp.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/v1/api/auth")
public class LoginController {

    @Autowired
    private AuthenticateService authenticateService;

    @PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public GlobalEntityResponse<LoginResponseWeb> login(@Valid @RequestBody LoginRequestWeb request) throws NotFoundException {
        return GlobalEntityResponse.ok(authenticateService.authenticate(LoginRequestApp.builder()
                .email(request.getEmail())
                .password(request.getPassword())
                .build()));
    }

    @GetMapping(value = "/google/redirect")
    public GlobalEntityResponse<String> gRedirect(){
        return GlobalEntityResponse.ok("it works");
    }

    @GetMapping(value = "/google/callback")
    public GlobalEntityResponse<String> gCallback(){
        return GlobalEntityResponse.ok("it works");
    }
}
