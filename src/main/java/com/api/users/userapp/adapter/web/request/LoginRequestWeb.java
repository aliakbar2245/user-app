package com.api.users.userapp.adapter.web.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;

@Setter
@Getter
public class LoginRequestWeb {
    @Email(message = "email format required")
    private String email;
    private String password;
}
