package com.api.users.userapp.adapter.app.in;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Sort;

@Builder
@Setter
@Getter
public class FetchUserRequestApp {
    private int page;
    private int size;
    private String sortBy;
    private Sort.Direction direction;
    private String email;
    private String name;
}
