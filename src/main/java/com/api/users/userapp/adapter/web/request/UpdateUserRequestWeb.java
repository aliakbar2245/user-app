package com.api.users.userapp.adapter.web.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Setter
@Getter
public class UpdateUserRequestWeb {
    @NotEmpty(message = "userId is required")
    @JsonProperty("id")
    private String userId;
    private String name;
    private String password;
}
