package com.api.users.userapp.adapter.app.repository;

import com.api.users.userapp.entity.UserEntity;
import com.api.users.userapp.entity.UserEntityInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, String>, JpaSpecificationExecutor<UserEntity> {
    @Query(value = "select id, name, email, password, expire_token_date\n" +
            "from users\n" +
            "where email = :email", nativeQuery = true)
    Optional<UserEntityInterface> findUserByEmail(@Param("email") String email);

    @Modifying
    @Query(value = "update users\n" +
            "set expire_token_date = :expDateToken\n" +
            "where id = :userId", nativeQuery = true)
    void updateUserExpiredDateToken(@Param("expDateToken") Date expDateToken,
                                    @Param("userId") String userId);

    @Query(value = "select id, name, email, password, expire_token_date\n" +
            "from users\n" +
            "where id = :userId", nativeQuery = true)
    Optional<UserEntityInterface> findUserById(@Param("userId") String userId);

    @Modifying
    @Query(value = "update users\n" +
            "set name = :name,\n" +
            "password = :password\n" +
            "where id = :userId", nativeQuery = true)
    void updateUserInformation(@Param("userId") String userId,
                               @Param("name") String name,
                               @Param("password") String password);

    @Modifying
    @Query(value = "update users\n" +
            "set deleted_date = :deletedDate\n" +
            "where id = :userId", nativeQuery = true)
    void softDeleteUserAccount(@Param("userId") String userId,
                               @Param("deletedDate") Date deletedDate);
}
