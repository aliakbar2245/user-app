package com.api.users.userapp.adapter.web.response;

import com.api.users.userapp.entity.UserEntity;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Builder
@Setter
@Getter
public class UserResponseWeb {
    private String id;
    private String name;
    private String email;
    private Date expireToken;

    public static UserResponseWeb fromEntity(UserEntity userEntity){
        return UserResponseWeb.builder()
                .id(userEntity.getId())
                .email(userEntity.getEmail())
                .name(userEntity.getName())
                .expireToken(userEntity.getExpireTokenDate())
                .build();
    }
}
