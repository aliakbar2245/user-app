package com.api.users.userapp.exceptions;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BadRequestException extends Exception {

    public BadRequestException(String message){
        super(message);
    }
}
