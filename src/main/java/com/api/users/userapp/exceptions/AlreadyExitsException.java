package com.api.users.userapp.exceptions;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AlreadyExitsException extends Exception {

    public AlreadyExitsException(String message){
        super(message);
    }
}
