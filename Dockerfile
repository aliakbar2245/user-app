FROM openjdk:8-jdk-alpine
WORKDIR /opt/app
COPY target/user-app-0.0.1-SNAPSHOT.jar userapp.jar
EXPOSE 8383
ENTRYPOINT ["java","-jar","userapp.jar"]
